package INF102.lab1.triplicate;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MyTriplicate<T> implements ITriplicate<T> {

    @Override
    public T findTriplicate(List<T> list) {
        Map<T, Integer> map = new HashMap<>();
        for (T num : list) {
            if (!map.containsKey(num)){
                map.put(num, 1);
                continue;
            } else {
                int current = map.get(num);
                if (current == 2){
                    return num;
                }
                map.put(num, current + 1);
            }
        }
        return null;
    }
    
}
